#--------------------------------------------------------#
# Pre-generate tables for given genesets  
# -------------------------------------------------------#

library(MonetDBLite)
library(DBI)
library(dplyr)
library(data.table)
library(ggplot2)
library(plotly)

setwd("~/HIRN/apps/scPancMeta") # or wherever app directory lives

source("utils.R")

# DB
monet_dbdir <- "monetdb/"
monet_con <- dbConnect(MonetDBLite::MonetDBLite(), monet_dbdir)

# Need UMAP table for cell types
UMAP <- readRDS("data/UMAP.rds")

hs_gs <- c("herrera_alpha_hs.txt", 
           "herrera_beta_hs.txt",
           "herrera_delta_hs.txt",
           "herrera_gamma_hs.txt")
hs_gs <- setNames(lapply(paste0("www/genesets/", hs_gs), scanGeneset), tools::file_path_sans_ext(hs_gs))
lengths(hs_gs)

# A number of genes do not exclusively belong to a set (which is OK, just see in case need these need to be highlighted?)
allgenes <- unlist(hs_gs)
dups <- allgenes[duplicated(allgenes)]
allgenes[allgenes %in% dups]

# Get data by geneset

datasets <- DBI::dbListTables(monet_con)
getGenesetX <- function(genes) lapply(datasets, function(dataset) tbl(monet_con, dataset) %>% filter(gene %in% genes) %>% collect() %>% as.data.table)
xtabs <- lapply(hs_gs, getGenesetX) # takes 2-3 mins
# Check data
# xtabs$herrera_alpha_hs[[1]] # baron
xtabs <- lapply(xtabs, function(genesetx) Map(function(.u, .x) merge(.u, .x, by = "cell", all.x = TRUE), UMAP, genesetx))
# Check data
# xtabs$herrera_alpha_hs$baron
for(x in seq_along(xtabs)) {
  dnames <- names(xtabs[[x]])
  for(d in seq_along(xtabs[[x]])) {
    xtabs[[x]][[d]][, dataset := dnames[d]]
    xtabs[[x]][[d]][, umap_1 := NULL]
    xtabs[[x]][[d]][, umap_2 := NULL]
  }
}
xtabs <- lapply(xtabs, rbindlist)
xtabs <- lapply(xtabs, function(dt) dt[is.na(x), x := 0])

xtabs_summary <- lapply(xtabs, function(dt) dt[celltype %in% c("Beta", "Alpha", "Delta", "Gamma"), .(normMeanExpr = mean(x)), 
                                               by = .(dataset, celltype, gene)])

# Plot aesthetics testing
# First version is closest to original specs
test <- ggplot(data = xtabs_summary[[1]], aes(x = dataset, y = normMeanExpr, color = celltype)) + geom_boxplot() + theme_bw()
ggplotly(test)
# There is too much overlap to look good, add facet_grid and test themes vs. default direct plot
test1 <- ggplot(data = xtabs_summary[[1]], aes(x = celltype, y = normMeanExpr, color = celltype)) + geom_boxplot() + facet_grid(~dataset) + theme_bw()
test2 <- ggplot(data = xtabs_summary[[1]], aes(x = celltype, y = normMeanExpr, color = celltype)) + geom_boxplot() + facet_grid(~dataset) + theme_light()
test3 <- ggplot(data = xtabs_summary[[1]], aes(x = celltype, y = normMeanExpr, color = celltype)) + 
  geom_boxplot(outlier.stroke = NULL, outlier.color = NULL,  alpha = 0.5) +
  facet_grid(~dataset) + theme_classic()
fig <- plot_ly(xtabs_summary[[1]], x = ~dataset, y = ~normMeanExpr, color = ~celltype, type = "box",
               text = ~gene, hoverinfo = "text") %>% 
  layout(boxmode = "group")


# Save data 
names(xtabs_summary) <- c("alpha", "beta", "delta", "gamma")
saveRDS(xtabs, "xtabs.rds")
saveRDS(xtabs_summary, "xtabs_summary.rds")

# Clean up
dbDisconnect(monet_con, shutdown = TRUE)
