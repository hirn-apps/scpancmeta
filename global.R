library(shiny)
library(shinythemes)
library(DBI)
library(MonetDBLite)
library(dplyr)
library(data.table)
library(plotly)
library(fgsea)
library(DT)

# geneset mod only
library(visNetwork) 
library(reactable)
library(tippy)

# App opts -----------------------------------------------------------------------------------#

# Set max upload size to 10MB per file
options(shiny.maxRequestSize = 10*1024^2)
options(warn=-1)

# Scripts- ------------------------------------------------------------------------------------#
source("plot2.R")
source("utils.R")
source("autoScript.R")

# DB/data ------------------------------------------------------------------------------------#

# First module
dbdir <- "monetdb"
SELECT_GENES <- readLines("data/SELECT_GENES.txt")
UMAP <- readRDS("data/UMAP.rds") 
datasets <- names(UMAP)

# Geneset module
load("data/geneset_table.rda")
load("data/geneset_graph.rda")
load("data/geneset_logFC.rda")

renderjs <- I("{ 
                 option: function(item, escape) { return '<div class = \"large ' + escape(item.label) + '\">' + escape(item.label) + '</div>'; },
                 item: function(item, escape) { return '<div class = \"large ' + escape(item.label) + '\">' + escape(item.label) + '</div>'; }
                }")
default_cell_choices <- setNames(c("all", "A", "B", "D", "G"), 
                                 c("all", "alpha", "beta", "delta", "gamma"))

# GSEA module
human <- c("herrera_alpha_hs.txt", 
           "herrera_beta_hs.txt",
           "herrera_delta_hs.txt",
           "herrera_gamma_hs.txt",
           "hallmark_pancreas_beta_cells_hs.grp",
           "reactome_beta_hs.txt",
           "wang_adult_beta_hs.txt")
mouse <- c("herrera_alpha_mmu.txt", 
           "herrera_beta_mmu.txt",
           "herrera_delta_mmu.txt",
           "herrera_gamma_mmu.txt",
           "hallmark_pancreas_beta_cells_mmu.grp", 
           "reactome_beta_mmu.txt")
genesets <- list()
genesets$human <- setNames(lapply(paste0("www/genesets/", human), scanGeneset), tools::file_path_sans_ext(human))
genesets$mouse <- setNames(lapply(paste0("www/genesets/", mouse), scanGeneset), tools::file_path_sans_ext(mouse))






