# Read geneset file -- all files have non-gene entries for first 2 lines and then one gene per line

scanGeneset <- function(file) scan(file, what = "character", skip = 2L)

# Checks ---------------------------------------------------------------------------------------#